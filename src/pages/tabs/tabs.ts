import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { ListsPage } from '../lists/lists';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ListsPage;

  constructor() {

  }
}
