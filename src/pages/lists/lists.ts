import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DbService } from '../../services/db.service';

/**
 * Generated class for the ListsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lists',
  templateUrl: 'lists.html'
})
export class ListsPage implements OnInit{
  public lists: Array<any>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCrtl: ModalController,
    public alertCtrl: AlertController,
    public _dbService: DbService
  ) {
    this.lists = [];
  }

  getLists (refresher = null) {
    this._dbService.getLists().subscribe(
      response => {
        this.lists = [... response['lists']]
        if (refresher) {
          refresher.complete();
        }
      },
      error => console.log(error.message)
    );
  }

  doRefresh (refresher) {
    this.getLists(refresher);
  }

  ngOnInit () {
    this.getLists();
  }

  deleteItem (list) {
    const confirm = this.alertCtrl.create({
      title: 'Borrar lista ' + list.name,
      message: "Seguro que quieres borrar la lista?",
      buttons: [
        { text: 'Cancelar', handler: data => {} },
        { text: 'Borrar', 
          handler: data => {
            this._dbService.removeList(list._id).subscribe(
              response => this.getLists()
            );
          } 
        }
      ]
    });
    confirm.present();
    
  }

  openPrompt () {
    const prompt = this.alertCtrl.create({
      title: 'Lista nueva',
      message: "Introduce un nombre para la lista",
      inputs: [{ name: 'name', placeholder: 'Nombre' }],
      buttons: [
        { text: 'Cancelar', handler: data => {} },
        { text: 'Guardar', 
          handler: data => {
            this._dbService.addList({name: data.name}).subscribe(
              response => this.getLists()
            )
          } 
        }
      ]
    });
    prompt.present();
  }

  goToPage(list) {
    console.log(list);
    this.navCtrl.push(ImeisPage, list);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListsPage');
  }

}

@Component({
  templateUrl: 'imeiList.html'
})
export class ImeisPage implements OnInit {
  public list;
  public imeis: Array<any>;

  selectedImage: string;
  imageText: string;

  constructor(
    private navParams: NavParams,
    private barcodeScanner: BarcodeScanner,
    private alertCtrl: AlertController,
    private _dbService: DbService
  ) {
    this.list = this.navParams.data;
    this.imeis = [];
  }

  getImeis (refresher = null) {
    this._dbService.getImeis(this.list._id).subscribe(
      res => {
        this.imeis = [... res['imeis']]
        if (refresher) {
          refresher.complete()
        }
      }
    )
  }
  doRefresh (refresher) {
    this.getImeis(refresher);
  }
  
  ngOnInit () {
    this.getImeis();
  }

  deleteImei (imei) {
    this._dbService.removeImei(imei._id).subscribe(
      res => this.getImeis()
    )
  }
  
  barCodeScan () {
    let options = {
      showTorchButton: true
    }
    this.barcodeScanner.scan(options).then(barcodeData => {
      console.log('Barcode data', barcodeData.format);
      let added = false;
      if (barcodeData.format === 'CODE_128') {
        this.imeis.forEach(el => {
          if (el.name === barcodeData.text) {
            const alert = this.alertCtrl.create({
              title: 'Error',
              subTitle: 'El IMEI ya existe',
              buttons: ['OK']
            });
            alert.present();
            added = true;
          }
        });
        if (!added) {
          this._dbService.addImei({name: barcodeData.text, list: this.list}).subscribe(
            res => this.getImeis()
          );
          const alert = this.alertCtrl.create({
            title: 'Hecho!',
            subTitle: 'IMEI añadido correctamente',
            buttons: ['OK']
          });
          alert.present();
        }
      } else {
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Error en el escaneado, inténtalo de nuevo',
          buttons: ['OK']
        });
        alert.present();
      }
    }).catch(err => {
        console.log('Error', err);
        const alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Error en la app, contacta con soporte',
          buttons: ['OK']
        });
        alert.present();
    });
  }

  manualAdd () {
    let added = false;
    const prompt = this.alertCtrl.create({
      title: 'IMEI',
      message: "Introduce un IMEI",
      inputs: [{ name: 'imei', placeholder: 'IMEI' }],
      buttons: [
        { text: 'Cancelar', handler: data => {} },
        { text: 'Guardar', 
          handler: data => {
            this.imeis.forEach(el => {
              if (el.name === data.imei) {
                const alert = this.alertCtrl.create({
                  title: 'Error',
                  subTitle: 'El IMEI ya existe',
                  buttons: ['OK']
                });
                alert.present();
                added = true;
              }
            });
            if (!added) {
              this._dbService.addImei({name: data.imei, list: this.list}).subscribe(
                res => this.getImeis()
              );
              const alert = this.alertCtrl.create({
                title: 'Hecho!',
                subTitle: 'IMEI añadido correctamente',
                buttons: ['OK']
              });
              alert.present();
            }
          } 
        }
      ]
    });
    prompt.present();
  }

}