import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class DbService {
    public url: string;
    public headers;
    constructor(private http: HttpClient){
        // this.url = 'http://0.0.0.0:3000/api/';
        this.url = 'http://api.barcodeapp.tk/api/';
        this.headers = new HttpHeaders({
            'Content-Type':  'application/json'
        });
    }
    getLists () {
        return this.http.get(this.url + 'getLists', {headers: this.headers});
    }
    addList (body) {
        return this.http.post(this.url + 'addList', body, {headers: this.headers});
    }
    removeList(id) {
        return this.http.delete(this.url + 'removeList/' + id);
    }
    getImeis (id) {
        return this.http.get(this.url + 'getImeis/' + id);
    }
    addImei (body) {
        return this.http.post(this.url + 'addImei', body, {headers: this.headers});
    }
    removeImei (id) {
        return this.http.delete(this.url + 'removeImei/' + id);
    }
}